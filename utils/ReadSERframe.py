import os,sys
import numpy as np


cfa=[[0,0],[0,1],[1,0],[1,1]]

def Run(infile,n=0,offset=-1,doplot=False,doconv=False,layer=-1):

    if isinstance(infile, dict):
        serheader=infile
        filename=serheader['filename']
    else:
        filename=infile
        import ReadSERheader
        serheader=ReadSERheader.Run(filename)

    W=serheader['ImageWidth']
    H=serheader['ImageHeight']
    PDPP=serheader['PixelDepthPerPlane']
    NoP=serheader['NumberOfPlanes']
    BPP=serheader['BytesPerPixel'] 
    frame_size = W * H * NoP

    if PDPP==8:
        imtype=np.uint8
    else:
        imtype=np.uint16

    
    if offset<0: #compute offset from frame number 
        read_size = frame_size * BPP
        offset=178+n*read_size

    with open(filename) as ser1:
        if NoP==1:
            data=np.reshape(np.fromfile(ser1,dtype=imtype,count=frame_size,offset=offset),(H,W))
        else:
            data=np.reshape(np.fromfile(ser1,dtype=imtype,count=frame_size,offset=offset),(H,W,NoP))

        if ((doconv) & (PDPP>8)):
            data=(data/255).astype('uint8')
        
        if (layer>-1):
            if (NoP>1):
                data=np.squeeze(data[:,:,layer]) #rgb
            else:
                data=data[cfa[layer][0]::2,cfa[layer][1]::2] #cfa


    
    if doplot:
        import matplotlib.pyplot as plt
        from matplotlib import cm
        plt.imshow(data,cmap=cm.get_cmap('gray'))
        plt.show()

    return data




if __name__ == "__main__":
    
    args=[]
    kwargs={}
    for a in sys.argv[1:]:
        if '=' in a:
            f,v=a.split('=')
            kwargs[f]=v
        else:
            args.append(a)
    Run(*tuple(args),**kwargs)
    # filename=R"C:\Users\Cissou\Pictures\Astro\SharpCap\M51\2020-04-23\lights\sattrack\sattrack.ser"
    # Run(filename,doplot=True)

