import os,sys
import numpy as np
import time



def cv2str(strbits):
    return ''.join([chr(item) for item in strbits])

def timestamp(ts):
    t1970_ms = (ts - 621355968000000000) / 10000
    secs = t1970_ms / 1000
    ms = t1970_ms % 1000
    try:
        return time.strftime('%Y-%m-%dT%H:%M:%S', time.gmtime(secs))+'.{:03d}'.format(int(ms))
    except:
        return '1970-00-00T00:00:00'



def CID(code):
    CIDdict={
        0:'MONO',
        8:'RGGB',
        9:'GRBG',
        10:'GBRG',
        11:'BGGR',
        16:'CYYM',
        17:'YCMY',
        18:'YMCY',
        19:'MYYC',
        100:'RGB',
        101:'BGR'
    }
    return CIDdict[code]

def NumberOfPlanes(ColorID):
    if len(ColorID)==3:
        return 3
    else:
        return 1

def BytesPerPixel(PixelDepthPerPlane):
    if PixelDepthPerPlane<=8:
        return 1
    else:
        return 2   


def Run(filename='',dbg=False):
    serheader={}
    with open(filename) as ser1:
        serheader['filename']=filename
        serheader['FileID']=cv2str(np.fromfile(ser1, dtype=np.uint8,count=14))
        serheader['LuID']=np.fromfile(ser1, dtype=np.int32,count=1)[0]
        serheader['ColorID']=CID(np.fromfile(ser1, dtype=np.int32,count=1)[0])
        serheader['NumberOfPlanes']=NumberOfPlanes(serheader['ColorID'])
        serheader['LittleEndian']=bool(np.fromfile(ser1, dtype=np.int32,count=1)[0])
        serheader['ImageWidth']=np.fromfile(ser1, dtype=np.int32,count=1)[0]
        serheader['ImageHeight']=np.fromfile(ser1, dtype=np.int32,count=1)[0]
        serheader['PixelDepthPerPlane']=np.fromfile(ser1, dtype=np.int32,count=1)[0]
        serheader['BytesPerPixel']=BytesPerPixel(serheader['PixelDepthPerPlane'])
        serheader['FrameCount']=np.fromfile(ser1, dtype=np.int32,count=1)[0]
        serheader['Observer']=cv2str(np.fromfile(ser1, dtype=np.uint8,count=40))
        serheader['Instrument']=cv2str(np.fromfile(ser1, dtype=np.uint8,count=40))
        serheader['Telescope']=cv2str(np.fromfile(ser1, dtype=np.uint8,count=40))
        serheader['DateTime']=timestamp(np.fromfile(ser1, dtype=np.int64,count=1)[0])
        serheader['DateTime_UTC']=timestamp(np.fromfile(ser1, dtype=np.int64,count=1)[0]) 

        trailerstart= serheader['FrameCount'] * serheader['ImageWidth'] * serheader['ImageHeight'] * serheader['BytesPerPixel']
        tss=np.fromfile(ser1, dtype=np.int64,count=serheader['FrameCount'],offset=trailerstart)
        serheader['Timestamps']=[timestamp(t) for t in tss]
        serheader['Timestampsd']=tss





    if dbg:
        print('Reading SER header')
        print('')
        for k in serheader.keys():
            if not 'timestamp' in k.lower():
                print(k+': '+str(serheader[k]))
        print('')

    return serheader


if __name__ == "__main__":
    
    args=[]
    kwargs={}
    for a in sys.argv[1:]:
        if '=' in a:
            f,v=a.split('=')
            kwargs[f]=v
        else:
            args.append(a)
    Run(*tuple(args),**kwargs)
    # filename=R"C:\Users\cisso\Pictures\astro\Sharpcap\M51\2020-04-23\light\Light_2020-04-23_22_49_51.ser"
    # filename=R"C:\Users\cisso\Pictures\astro\Sharpcap\M51\2020-04-23\light\sattrack\sattrack.ser"
    # Run(filename,dbg=True)

