import os,sys
from astropy.io import fits
import PySimpleGUI as sg
import configparser
from pydoc import locate
import threading 
import time
from shutil import copyfile
import numpy as np

if __package__ is None or len(__package__)==0:
    sys.path.append(".")

from utils import ReadSERframe
from utils import ReadSERheader

#################################################
# GLOBALS

INIFILE='picfeeder.ini'
OPT= {'folderin': '',
    'folderout': '',
    'interval': 0.0}

DELETE_ICO = b'iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAABmJLR0QA/wD/AP+gvaeTAAAA1klEQVRoge2Y0Q3CMAwFT8AKRDBmV4UPxik/jYhQWzWt3TjSuwHsd1JrJwEhhBBCiFMZgORQN021XRmAEXhjK5GmmiPOEmUjKwmPmqvcgdfU8AM8DWs9Dqfb2XiPRLPwSwFqJJqHXwqyRSJM+EyNRLjwmS0SYcNn1iTCh8/MSXQTPvO/mE5dUlaUEq7hLx5F+QXP3ICrUy9zuv6E5n5Yy7OTK2vTJrzEllEZVqJmzoeT2LOkwkgc2bDNJSyOB80kLM82p0t0f6nv/lkFOn/YEkIIIYQo+AK01II4VuO91QAAAABJRU5ErkJggg=='
PAUSE_ICO = b'iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAABmJLR0QA/wD/AP+gvaeTAAAAZklEQVRoge3YQQrAIAxFQVu8/5XbExRibfkKM2sJPLNLawBMOIrvrlXnncVBy+qD759+pPqjn8/bfgMC0gSkCUgTkCYgTUCagDQBaQLSBKQJSBOQJiBNQNrodfrtFfq3edtvAIApNzKuBkZuD1O/AAAAAElFTkSuQmCC'
PLAY_ICO = b'iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAABmJLR0QA/wD/AP+gvaeTAAABE0lEQVRoge2ZPQrCQBBGH4J6B0E8i4WFp7G19Cy2ll7FQjyGaB0LHQiy6uZnd2biPgghMZDvg5e4u4FCoRDDFphoh+hCBVyAtXaQtlS17QDMdeM0R8LfantXWkmBGbCvHbvRSgILS+BUO38EFgq5onkvADAGNsD19dsd2AHTrMkiCRUQXGj1rYBgWquYAmBYq9gCgjmtmhYQzGjVtgAY0apLAUFVqz4KCCpa9VkAFLTqu4CQTatUBYTkWqUuAIm1ylFAmPOcNMk9z8Dq08WjTKHUKQr9wO1D7PY16vqPzO1Qwu1gzvVw2u2Exu2U0oQuIQa9rGJOlxCDW1o0rUuIwSyvu//AUeH4E5MbXUK40qVQ+CcersfNljGqeacAAAAASUVORK5CYII='
STOP_ICO = b'iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAABmJLR0QA/wD/AP+gvaeTAAAAa0lEQVRoge3ZsQqAMAwA0Vj8/1/WrYuEOEjOwL2xXXKkWyMkjXYk51frFO895l3EFF86i/tsQ93SFzF+AwbQDKAZQDOAZgDNAJoBNANoBtAMoBlAM4BmAM0AmgE0A2jVD81f/8q28RuQNNwNgfsDT6PnSWAAAAAASUVORK5CYII='
ALLOWED_EXT=['.fits','.fit','.fts','.ser']
#################################################
# HELPERS

def get_ini_name(basename=INIFILE):
    if os.name == 'nt':
        myhome=os.path.join(os.environ.get('USERPROFILE'))
    else:
        myhome=os.path.join(os.environ.get('HOME'))
    ini_file=os.path.join(myhome,basename)
    return ini_file

def read_ini_file(opt=OPT):
    inifile=get_ini_name()
    config = configparser.ConfigParser()
    config['Base']=opt
    if os.path.isfile(inifile):
        config.read(inifile)
    else: #creating ini file
        with open(inifile, 'w') as fp: 
            config.write(fp)
    ret=dict(config['Base'])
    for k,v in ret.items(): #recasting to original data type
        t=locate(type(opt[k]).__name__)
        ret[k]=t(v)
    return ret

def write_ini_file(opt,basename=INIFILE):
    inifile=get_ini_name()
    config = configparser.ConfigParser()
    config['Base']=opt
    with open(inifile, 'w') as configfile:
        config.write(configfile)

def get_content(folder):
    out={'files':[],'mess':'Empty','nbframes':0,'ext':'.fits','searheader':None}
    files_valid=[]
    exts=ALLOWED_EXT
    if len(folder)==0: return out
    if os.path.isdir(folder):
        files=os.listdir(folder)
        files_valid=[]
        first=True
        for f in files:
            ext=os.path.splitext(f)[1]
            if ext.lower() in exts:
                files_valid.append(os.path.join(folder,f))
                if first: #force to find only files of the same type as first one
                    exts=[ext]
                    first=False
                if ext=='.ser':
                    out['serheader']=ReadSERheader.Run(files_valid[0])
                    break #break if first file is a ser - will deal with only one ser

    out['files']=sorted(files_valid, key=lambda t: os. stat(t).st_mtime)
    if exts[0]=='.ser':
        out['nbframes']=out['serheader']['FrameCount']
    else:
        out['nbframes']=len(files_valid)
    out['mess']='Number of frames:{: 4d}'.format(out['nbframes'])
    out['ext']=exts[0]
    # global COUNTER
    # COUNTER=out['nbframes']
    return out

def warningwd(warntext):
    return sg.popup_ok_cancel('Warning'+': '+warntext,title='Warning')

def confirm_cleaning(folderinfo):
    if len(folderinfo['files'])==0: return folderinfo
    files=folderinfo['files']
    folder=os.path.split(files[0])[0]
    popup=warningwd('Confirm to empty receiving folder')
    if popup.lower()=='ok':
        for f in files:
            os.remove(f)
        global COUNTER
        COUNTER=0
        return get_content(folder)
    else:
        return folderinfo

def disable_play(window):
    if len(window['folderininfo'].metadata['files'])==0: return True # no input files to be copied
    if opt['interval']<0: return True # negative interval
    if len(opt['folderout'])==0: return True # no folder to copy to
    if IS_STREAMING: return True
    return False

def disable_delete(window):
    if len(window['folderoutinfo'].metadata['files'])==0: return True # no output files to be cleared
    if len(opt['folderout'])==0: return True # no folder to clear
    if IS_STREAMING: return True    
    return False

def disable_pause(window):
    return not(IS_STREAMING)

def disable_stop(window):
    return not(IS_STREAMING)   

def check_folders(window):
    if IS_STREAMING: 
        window['folderoutlibbrowse'].Update(disabled=True)
        window['folderinlibbrowse'].Update(disabled=True)
        window['folderout'].Update(disabled=True)
        window['folderin'].Update(disabled=True)
        window['interval'].Update(disabled=True)
    else:
        window['folderoutlibbrowse'].Update(disabled=False)
        window['folderinlibbrowse'].Update(disabled=False)
        window['folderout'].Update(disabled=False)
        window['folderin'].Update(disabled=False)
        window['interval'].Update(disabled=False)

def _addbutton(key,icon='',disabled=True):
    if len(icon)==0: icon=eval(key.upper()+'_ICO')
    return sg.Button('', image_data=icon,
          button_color=(sg.theme_background_color(),sg.theme_background_color()),
          border_width=0, key=key,disabled=disabled)

def copy_frames(): # function that copies frames to folder out
    global IS_STREAMING,COUNTER,RUNNING,WAIT_TIME,window
    print('Copying thread started')
    while RUNNING:
        if IS_STREAMING:
            infoin=window['folderininfo'].metadata
            infoout=window['folderoutinfo'].metadata
            if infoin['nbframes']-1<COUNTER:
                print('No more frames to copy')
                IS_STREAMING=False
                check_folders(window)
                window['delete'].Update(disabled=disable_delete(window)) 
                window['pause'].Update(disabled=disable_pause(window))
                continue
            if WAIT_TIME>0:
                time.sleep(WAIT_TIME)
            # print(COUNTER)
            fout=[]
            if infoin['ext']=='.ser':
                serheader=infoin['serheader']
                print('in:{:s}'.format(serheader['filename']))
                data=np.flipud(ReadSERframe.Run(serheader,n=COUNTER))
                hdr = fits.Header()
                if serheader['ColorID'] in ['RGGB','GRBG','GBRG','BGGR']:
                    hdr['BAYERPAT']=serheader['ColorID']
                hdr['DATE-OBS']=serheader['Timestamps'][COUNTER]
                hdr['ROWORDER']='BOTTOM-UP'
                hdu = fits.PrimaryHDU(data,header=hdr)
                fout=infoin['files'][0].replace(window['folderin'].Get(),window['folderout'].Get())
                fout=fout.replace('.ser','_{:05d}{:s}'.format(COUNTER,ALLOWED_EXT[0]))
                hdu.writeto(fout)
            else:
                fin=infoin['files'][COUNTER]
                fout=infoin['files'][COUNTER].replace(window['folderin'].Get(),window['folderout'].Get())
                print('in:{:s}'.format(fin))
                copyfile(fin,fout)
            if fout:
                print('out:{:s}'.format(fout))
                infoout['files']+=fout
                infoout['nbframes']+=1
                infoout['mess']='Number of frames:{: 4d}'.format(infoout['nbframes'])
                window['folderoutinfo'].metadata=infoout
                window['folderoutinfo'].Update(infoout['mess'])
            COUNTER+=1
    print('Copying thread stopped')

#####################################################################
# MAIN

IS_STREAMING=False
COUNTER=0
RUNNING=False
opt=read_ini_file()
WAIT_TIME=opt['interval']


sg.theme('Light Blue 3')
tooltipsdict={
    'folderin': 'path to folder containing images. Finds extension of the first file and only search for other files with same type',
    'folderout': 'path to folder being monitored by livestacking',
    'interval': 'waiting time before copying next image in s, 0 to feed continuously',
    }
msgdict={
    'folderin': 'Folder in',
    'folderout': 'Folder out',
    'interval': 'Interval [s]',
    }

layout=[]
# Inputs
layout+=[[sg.Text('Folders', font='Any 12')]]
name='folderin'
layout+=[[sg.Text(msgdict[name],size=(10,1)),sg.Input(opt[name],key=name,enable_events=True,tooltip=tooltipsdict[name],size=(75,1)),sg.FolderBrowse(target=name,key=name+'libbrowse',tooltip=tooltipsdict[name])]]
folderininfo=get_content(opt['folderin'])
layout+=[[sg.Text('',size=(10,1)),sg.Text(folderininfo['mess'],size=(66,1),key='folderininfo',metadata=folderininfo)]]
name='folderout'
layout+=[[sg.Text(msgdict[name],size=(10,1)),sg.Input(opt[name],key=name,enable_events=True,tooltip=tooltipsdict[name],size=(75,1)),sg.FolderBrowse(target=name,key=name+'libbrowse',tooltip=tooltipsdict[name])]]
folderoutinfo=get_content(opt['folderout'])
layout+=[[sg.Text('',size=(10,1)),sg.Text(folderoutinfo['mess'],size=(66,1),key='folderoutinfo',metadata=folderoutinfo)]]
name='interval'
layout+=[[sg.Text(msgdict[name], size=(10,1)),
                sg.Input(opt[name],key=name,tooltip=tooltipsdict[name],size=(5,1),enable_events=True)]]
layout+=[[sg.HorizontalSeparator()]]

# Buttons
layout+=[[_addbutton('play'),_addbutton('pause'),_addbutton('delete')]]        


# loading window
window = sg.Window('Livestacking feeder', layout,finalize=True)  
folderinfo=get_content(opt['folderout'])
folderinfo=confirm_cleaning(folderinfo)
window['folderoutinfo'].Update(folderinfo['mess'])
window['folderoutinfo'].metadata=folderinfo
COUNTER=folderinfo['nbframes']


window['play'].Update(disabled=disable_play(window))
window['delete'].Update(disabled=disable_delete(window))


#starting flag and copying thread
RUNNING=True
loop=threading.Thread(target=copy_frames,daemon=True)
loop.start()

while True: 
    event, values = window.read()  
    if event == sg.WIN_CLOSED:   
        RUNNING=False
        time.sleep(0.5) # giving some time for the copying thread to stop
        break   # always,  always give a way out!   
    if event=='folderin':
        folderinfo=get_content(values['folderin'])
        window['folderininfo'].Update(folderinfo['mess'])
        window['folderininfo'].metadata=folderinfo
        opt['folderin']=values['folderin']

    if event=='folderout':
        if values['folderout']==values['folderin']:
            sg.popup_ok('Error: cannot copy to the same folder as folder in',title='Error')
            window['folderout'].Update('')
        else:
            folderinfo=get_content(values['folderout'])
            folderinfo=confirm_cleaning(folderinfo)
            window['folderoutinfo'].Update(folderinfo['mess'])
            window['folderoutinfo'].metadata=folderinfo
            opt['folderout']=values['folderout']
    
    if event=='interval':
        try:
            opt['interval']=float(values['interval'])
        except:
            opt['interval']=0.0
        WAIT_TIME=opt['interval']

    if event=='delete':
        folderinfo=get_content(values['folderout'])
        folderinfo=confirm_cleaning(folderinfo)
        window['folderoutinfo'].Update(folderinfo['mess'])
        window['folderoutinfo'].metadata=folderinfo

    if event=='play':
        IS_STREAMING=True 
    if event in ['pause']:
        IS_STREAMING=False  
    
    write_ini_file(opt)
    check_folders(window)
    window['play'].Update(disabled=disable_play(window))
    window['delete'].Update(disabled=disable_delete(window)) 
    window['pause'].Update(disabled=disable_pause(window))

    

