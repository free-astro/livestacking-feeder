import os,sys
from astropy.io import fits
import PySimpleGUI as sg
import configparser
from pydoc import locate
import threading 
import time
from shutil import copyfile,rmtree
import numpy as np
from pysiril.siril import Siril
from pysiril.wrapper import Wrapper
from astropy.io.fits.convenience import getheader

if __package__ is None or len(__package__)==0:
    sys.path.append(".")

from utils import ReadSERframe
from utils import ReadSERheader

#################################################
# GLOBALS

INIFILE='LLS.ini'
OPT= {'folderin': '',
    'masterdark': '',
    'masterflat': '',
    'keep': True,
    'folderout': ''
    }

DELETE_ICO = b'iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAABmJLR0QA/wD/AP+gvaeTAAAA1klEQVRoge2Y0Q3CMAwFT8AKRDBmV4UPxik/jYhQWzWt3TjSuwHsd1JrJwEhhBBCiFMZgORQN021XRmAEXhjK5GmmiPOEmUjKwmPmqvcgdfU8AM8DWs9Dqfb2XiPRLPwSwFqJJqHXwqyRSJM+EyNRLjwmS0SYcNn1iTCh8/MSXQTPvO/mE5dUlaUEq7hLx5F+QXP3ICrUy9zuv6E5n5Yy7OTK2vTJrzEllEZVqJmzoeT2LOkwkgc2bDNJSyOB80kLM82p0t0f6nv/lkFOn/YEkIIIYQo+AK01II4VuO91QAAAABJRU5ErkJggg=='
PAUSE_ICO = b'iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAABmJLR0QA/wD/AP+gvaeTAAAAZklEQVRoge3YQQrAIAxFQVu8/5XbExRibfkKM2sJPLNLawBMOIrvrlXnncVBy+qD759+pPqjn8/bfgMC0gSkCUgTkCYgTUCagDQBaQLSBKQJSBOQJiBNQNrodfrtFfq3edtvAIApNzKuBkZuD1O/AAAAAElFTkSuQmCC'
PLAY_ICO = b'iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAABmJLR0QA/wD/AP+gvaeTAAABE0lEQVRoge2ZPQrCQBBGH4J6B0E8i4WFp7G19Cy2ll7FQjyGaB0LHQiy6uZnd2biPgghMZDvg5e4u4FCoRDDFphoh+hCBVyAtXaQtlS17QDMdeM0R8LfantXWkmBGbCvHbvRSgILS+BUO38EFgq5onkvADAGNsD19dsd2AHTrMkiCRUQXGj1rYBgWquYAmBYq9gCgjmtmhYQzGjVtgAY0apLAUFVqz4KCCpa9VkAFLTqu4CQTatUBYTkWqUuAIm1ylFAmPOcNMk9z8Dq08WjTKHUKQr9wO1D7PY16vqPzO1Qwu1gzvVw2u2Exu2U0oQuIQa9rGJOlxCDW1o0rUuIwSyvu//AUeH4E5MbXUK40qVQ+CcersfNljGqeacAAAAASUVORK5CYII='
STOP_ICO = b'iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAABmJLR0QA/wD/AP+gvaeTAAAAa0lEQVRoge3ZsQqAMAwA0Vj8/1/WrYuEOEjOwL2xXXKkWyMkjXYk51frFO895l3EFF86i/tsQ93SFzF+AwbQDKAZQDOAZgDNAJoBNANoBtAMoBlAM4BmAM0AmgE0A2jVD81f/8q28RuQNNwNgfsDT6PnSWAAAAAASUVORK5CYII='
ALLOWED_EXT=['.fits','.fit','.fts']
#################################################
# HELPERS

def get_ini_name(basename=INIFILE):
    if os.name == 'nt':
        myhome=os.path.join(os.environ.get('USERPROFILE'))
    else:
        myhome=os.path.join(os.environ.get('HOME'))
    ini_file=os.path.join(myhome,basename)
    return ini_file

def read_ini_file(opt=OPT):
    inifile=get_ini_name()
    config = configparser.ConfigParser()
    config['Base']=opt
    if os.path.isfile(inifile):
        config.read(inifile)
    else: #creating ini file
        with open(inifile, 'w') as fp: 
            config.write(fp)
    ret=dict(config['Base'])
    for k,v in ret.items(): #recasting to original data type
        t=locate(type(opt[k]).__name__)
        ret[k]=t(v)
    return ret

def write_ini_file(opt,basename=INIFILE):
    inifile=get_ini_name()
    config = configparser.ConfigParser()
    config['Base']=opt
    with open(inifile, 'w') as configfile:
        config.write(configfile)

def get_content(folder):
    out={'files':[],'mess':'Empty','nbframes':0,'ext':'.fits','searheader':None}
    files_valid=[]
    exts=ALLOWED_EXT
    if len(folder)==0: return out
    if os.path.isdir(folder):
        files=os.listdir(folder)
        files_valid=[]
        first=True
        for f in files:
            ext=os.path.splitext(f)[1]
            if ext.lower() in exts:
                files_valid.append(os.path.join(folder,f))
                if first: #force to find only files of the same type as first one
                    exts=[ext]
                    first=False
                if ext=='.ser':
                    out['serheader']=ReadSERheader.Run(files_valid[0])
                    break #break if first file is a ser - will deal with only one ser

    out['files']=sorted(files_valid, key=lambda t: os. stat(t).st_mtime)
    if exts[0]=='.ser':
        out['nbframes']=out['serheader']['FrameCount']
    else:
        out['nbframes']=len(files_valid)
    out['mess']='Number of frames:{: 4d}'.format(out['nbframes'])
    out['ext']=exts[0]
    return out

def warningwd(warntext):
    return sg.popup_ok_cancel('Warning'+': '+warntext,title='Warning')

def confirm_cleaning(folderinfo):
    if len(folderinfo['files'])==0: return folderinfo
    files=folderinfo['files']
    folder=os.path.split(files[0])[0]
    popup=warningwd('Confirm to empty receiving folder')
    if popup.lower()=='ok':
        for f in files:
            os.remove(f)
        global COUNTER
        COUNTER=0
        return get_content(folder)
    else:
        return folderinfo

def disable_play(window):
    if len(window['folderin'].Get()) == 0: return True # no folder to monitor
    # if len(window['folderininfo'].metadata['files'])==0: return True # no input files to be copied
    if IS_STREAMING: return True
    return False

def disable_delete(window):
    if len(window['folderoutinfo'].metadata['files'])==0: return True # no output files to be cleared
    if IS_STREAMING: return True    
    return False

def disable_pause(window):
    return not(IS_STREAMING)
  

def check_folders(window):
    global IS_STREAMING, STACKED
    needupdate = ['folderoutlibbrowse','folderinlibbrowse','masterdarklibbrowse','masterflatlibbrowse','masterdark','masterflat','folderout','folderin','keep']
    for n in needupdate:
        window[n].Update(disabled=(IS_STREAMING|(STACKED>0)))

def _addbutton(key,icon='',disabled=True):
    if len(icon)==0: icon=eval(key.upper()+'_ICO')
    return sg.Button('', image_data=icon,
          button_color=(sg.theme_background_color(),sg.theme_background_color()),
          border_width=0, key=key,disabled=disabled)

def CreateSeqFile(seqfile, img_nb_list) :
    seqname=os.path.splitext(os.path.split(seqfile)[1])[0]
    with open(seqfile, 'w', newline='') as fd :
        fd.write('#Siril sequence file. Contains list of files (images), selection, and registration data\n')
        fd.write("#S 'sequence_name' start_index nb_images nb_selected fixed_len reference_image version\n")
        fd.write("S '" + seqname + "' " + str(img_nb_list[0]) + " " + str(len(img_nb_list)) + " " + str(len(img_nb_list)) + " 5 -1 1\n")
        fd.write("L -1\n")
        for num in img_nb_list:
            fd.write("I " + str(num) + " 1\n")

def stack_frames(): # function that stack frames to folder out
    global IS_STREAMING,COUNTER,RUNNING,WAIT_TIME,APP,PROCESSFOLDER,STACKFOLDER,STACKED,CMD,window
    print('Stacking thread started')
    needsupdate = False
    while RUNNING:
        if IS_STREAMING:
            if APP==0:
                #cleaning temp folders
                if os.path.isdir(os.path.join(window['folderin'].Get(),PROCESSFOLDER)):
                    rmtree(os.path.join(window['folderin'].Get(),PROCESSFOLDER))
                os.makedirs(os.path.join(window['folderin'].Get(),PROCESSFOLDER))
                os.makedirs(os.path.join(window['folderin'].Get(),STACKFOLDER),exist_ok = True)
                
                tmpstack = window['folderout'].Get()
                storetmpstack=False
                if len(tmpstack)>0:
                    storetmpstack=True
                    if os.path.isdir(tmpstack):
                        rmtree(tmpstack)
                    os.makedirs(tmpstack)
                    tempname=os.path.split(tmpstack)[1]

                print('Starting Siril')
                APP=Siril(requires='1.0.0')
                APP.Open()
                CMD=Wrapper(APP)


            if WAIT_TIME>0:
                time.sleep(WAIT_TIME)
            window['folderininfo'].metadata=get_content(window['folderin'].Get())
            infoin=window['folderininfo'].metadata
            window['folderininfo'].Update(infoin['mess'])
            infoout=window['folderoutinfo'].metadata
            COUNTER=len(infoin['files'])
            # print(COUNTER)
            
            ext=infoin['ext']
            if infoin['nbframes']>1:
                if STACKED == 0: #first stacking - run the full show
                    processfolder=os.path.normpath(os.path.join(window['folderin'].Get(),'./{:s}'.format(PROCESSFOLDER)))
                    stackfolder=os.path.normpath(os.path.join(window['folderin'].Get(),'./{:s}'.format(STACKFOLDER)))
                    # CMD.setcpu(1) # only one thread
                    CMD.setext(ext[1:])
                    CMD.cd(window['folderin'].Get())
                    CMD.convert('LIGHT',out='./{:s}'.format(PROCESSFOLDER))
                    CMD.cd('./{:s}'.format(PROCESSFOLDER))
                    ppdict={}
                    prefix='pp_'
                    ppdict['prefix']=prefix
                    dark=window['masterdark'].Get()
                    if len(dark)>0:
                        ppdict['dark']=dark
                    flat=window['masterflat'].Get()
                    if len(flat)>0:
                        ppdict['flat']=flat
                    hdr=getheader(infoin['files'][0])
                    if len(hdr)>0:
                        if 'BAYERPAT' in hdr.keys():
                            if len(flat)>0:
                                ppdict['cfa']=True
                                ppdict['equalize_cfa']=True
                            ppdict['debayer']=True
                    if len(ppdict)==1: #mono with no darks nor flats so no preprocessing
                        prefix=''
                    else:
                        CMD.preprocess('LIGHT',**ppdict)
                    CMD.register(prefix+'LIGHT')
                    CMD.stack('r_'+prefix+'LIGHT',type='mean',rejection_type='w',sigma_high=100, sigma_low=100,norm='no',out='../{:s}/{:s}'.format(STACKFOLDER,STACKFOLDER))
                    CMD.close()
                    CMD.cd(window['folderin'].Get())
                    STACKED=len(infoin['files'])
                    if storetmpstack:
                        copyfile(os.path.join(stackfolder,STACKFOLDER+ext),os.path.join(tmpstack,'{:s}_{:05d}_{:05d}{:s}'.format(tempname,1,STACKED,ext)))
                    needsupdate=True

                elif (STACKED < COUNTER):
                    CMD.cd(window['folderin'].Get())
                    CMD.convert('LIGHT',out='./{:s}'.format(PROCESSFOLDER))
                    CMD.cd('./{:s}'.format(PROCESSFOLDER))
                    seqfile = os.path.join(window['folderin'].Get(),PROCESSFOLDER,'LIGHT_.seq')
                    img_list=np.arange(STACKED+1, COUNTER+1, step=1, dtype=int)
                    CreateSeqFile(seqfile, img_list)
                    if len(ppdict)>1: #not mono with no darks nor flats so no preprocessing
                        CMD.preprocess('LIGHT',**ppdict)
                    seqfile = os.path.join(window['folderin'].Get(),PROCESSFOLDER,'pp_LIGHT_.seq')
                    img_list_with_first= np.insert(img_list,0,[1])
                    CreateSeqFile(seqfile, img_list_with_first)
                    CMD.register(prefix+'LIGHT')
                    if COUNTER - STACKED > 1:
                        seqfile = os.path.join(window['folderin'].Get(),PROCESSFOLDER,'r_pp_LIGHT_.seq')
                        CreateSeqFile(seqfile, img_list)
                        CMD.stack('r_'+prefix+'LIGHT',type='mean',rejection_type='w',sigma_high=100, sigma_low=100,norm='no',out='../{:s}/{:s}'.format(STACKFOLDER,'tmp'))
                    else: # there is a single file for this iteration, just copy as tmp
                        if os.path.exists(os.path.join(stackfolder,'tmp'+ext)): os.remove(os.path.join(stackfolder,'tmp'+ext))
                        os.symlink(os.path.join(processfolder,'r_{:s}LIGHT_{:05d}{:s}'.format(prefix,COUNTER,ext)),os.path.join(stackfolder,'tmp'+ext))
                    # adding image with respective weights
                    CMD.cd('../{:s}'.format(STACKFOLDER))
                    N=STACKED
                    n=COUNTER-STACKED
                    # weight on temp = n/(N+n)
                    CMD.load('tmp'+ext)
                    CMD.fmul(float(n/(N+n)))
                    CMD.save('tmp'+ext)
                    # weight on stack = N/(N+n)
                    CMD.load(STACKFOLDER+ext)
                    CMD.fmul(float(N/(N+n)))
                    CMD.iadd('tmp'+ext)
                    CMD.save(STACKFOLDER+ext)
                    CMD.close()
                    STACKED=COUNTER
                    if storetmpstack:
                        copyfile(os.path.join(stackfolder,STACKFOLDER+ext),os.path.join(tmpstack,'{:s}_{:05d}_{:05d}{:s}'.format(tempname,1,STACKED,ext)))
                    os.remove(os.path.join(stackfolder,'tmp'+ext))
                    needsupdate=True

                if needsupdate:
                    infoout['files']=infoin['files'][0:COUNTER+1]
                    infoout['nbframes']=len(infoout['files'])
                    STACKED=infoout['nbframes']
                    infoout['mess']='Number of frames:{: 4d}'.format(infoout['nbframes'])
                    window['folderoutinfo'].metadata=infoout
                    window['folderoutinfo'].Update(infoout['mess'])
                    needsupdate=False
            else:
                window['delete'].Update(disabled=(STACKED==0))
        a = 0
    print('Stacking thread stopped')

#####################################################################
# MAIN

IS_STREAMING=False
COUNTER=0
RUNNING=False
opt=read_ini_file()
WAIT_TIME=1.
STACKED=0
APP=0
CMD=0
PROCESSFOLDER='process'
STACKFOLDER='result'


sg.theme('Light Blue 3')
tooltipsdict={
    'folderin': 'path to folder containing images to stack as they arrive',
    'masterdark': 'path to masterdark',
    'masterflat': 'path to masterdark',
    'keep': 'keep intermediate stacks',
    'folderout': 'intermediate stacks storage path'
    }
msgdict={
    'folderin': 'Folder to monitor',
    'masterdark': 'Masterdark path',
    'masterflat': 'Masterflat path',
    'keep': 'Keep intermediate stacks',
    'folderout': 'Intermediate stacks'
    }

layout=[]
# Inputs
layout+=[[sg.Text('Folder', font='Any 12')]]

name='folderin'
layout+=[[sg.Text(msgdict[name],size=(15,1)),sg.Input(opt[name],key=name,enable_events=True,tooltip=tooltipsdict[name],size=(75,1)),sg.FolderBrowse(target=name,key=name+'libbrowse',tooltip=tooltipsdict[name])]]
folderininfo=get_content(opt['folderin'])
layout+=[[sg.Text('',size=(15,1)),sg.Text(folderininfo['mess'],size=(66,1),key='folderininfo',metadata=folderininfo)]]

layout+=[[sg.HorizontalSeparator(pad=(0,10))]]
layout+=[[sg.Text('Masters', font='Any 12')]]

name='masterdark'
layout+=[[sg.Text(msgdict[name],size=(15,1)),sg.Input(opt[name],key=name,enable_events=True,tooltip=tooltipsdict[name],size=(75,1)),sg.FileBrowse(target=name,key=name+'libbrowse',tooltip=tooltipsdict[name])]]

name='masterflat'
layout+=[[sg.Text(msgdict[name],size=(15,1)),sg.Input(opt[name],key=name,enable_events=True,tooltip=tooltipsdict[name],size=(75,1)),sg.FileBrowse(target=name,key=name+'libbrowse',tooltip=tooltipsdict[name])]]

layout+=[[sg.HorizontalSeparator(pad=(0,10))]]
layout+=[[sg.Text('Stacking', font='Any 12')]]

name='keep'
layout+=[[sg.Checkbox(msgdict[name],opt[name],key=name,tooltip=tooltipsdict[name],enable_events=True)]]

name='folderout'
layout+=[[sg.Text(msgdict[name],size=(15,1)),sg.Input(opt[name],key=name,enable_events=True,tooltip=tooltipsdict[name],size=(75,1)),sg.FolderBrowse(target=name,key=name+'libbrowse',tooltip=tooltipsdict[name])]]
folderoutinfo=get_content(opt['folderout'])
layout+=[[sg.Text('Stacked:',size=(15,1)),sg.Text(folderoutinfo['mess'],size=(66,1),key='folderoutinfo',metadata=folderoutinfo)]]

layout+=[[sg.HorizontalSeparator()]]

# Buttons
layout+=[[_addbutton('play'),_addbutton('pause'),_addbutton('delete')]]


# loading window
window = sg.Window('Light LiveStacker', layout,finalize=True)  
folderinfo=get_content(opt['folderout'])
folderinfo=confirm_cleaning(folderinfo)
window['folderoutinfo'].Update(folderinfo['mess'])
window['folderoutinfo'].metadata=folderinfo
COUNTER=folderinfo['nbframes']


window['play'].Update(disabled=disable_play(window))
window['delete'].Update(disabled=disable_delete(window))


#starting flag and stacking thread
RUNNING=True
loop=threading.Thread(target=stack_frames,daemon=True)
loop.start()

while True: 
    event, values = window.read()
    if event == sg.WIN_CLOSED:
        RUNNING=False
        if isinstance(APP, Siril): APP.Close()
        del APP
        time.sleep(0.5) # giving some time for the stacking thread to stop
        break   # always,  always give a way out!   
    if event=='folderin':
        folderinfo=get_content(values['folderin'])
        window['folderininfo'].Update(folderinfo['mess'])
        window['folderininfo'].metadata=folderinfo
        opt['folderin']=values['folderin']

    if event=='folderout':
        if values['folderout']==values['folderin']:
            sg.popup_ok('Error: cannot stack to the same folder as folder in',title='Error')
            window['folderout'].Update('')
        else:
            folderinfo=get_content(values['folderout'])
            folderinfo=confirm_cleaning(folderinfo)
            window['folderoutinfo'].Update(folderinfo['mess'])
            window['folderoutinfo'].metadata=folderinfo
            opt['folderout']=values['folderout']

    if event=='masterdark':
        opt['masterdark']=values['masterdark']
    if event=='masterflat':
        opt['masterflat']=values['masterflat']

    if event=='delete':
        folderinfo=get_content(values['folderout'])
        folderinfo=confirm_cleaning(folderinfo)
        window['folderoutinfo'].Update(folderinfo['mess'])
        window['folderoutinfo'].metadata=folderinfo
        COUNTER=0
        STACKED=0

    if event=='play':
        IS_STREAMING=True
    if event in ['pause']:
        IS_STREAMING=False
    write_ini_file(opt)
    check_folders(window)
    window['play'].Update(disabled=disable_play(window))
    window['delete'].Update(disabled=disable_delete(window))
    window['pause'].Update(disabled=disable_pause(window))

    

